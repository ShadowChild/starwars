package shadowchild.mods.starwars.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.obj.WavefrontObject;


/**
 * Author: ShadowChild.
 */
public class ModelBlasterRifle extends ModelBase {

    private WavefrontObject rifle = (WavefrontObject)AdvancedModelLoader.loadModel(new ResourceLocation("starwars:models/weapon/testHighResModel.obj"));

    public void render() {

        rifle.renderAll();
        // super.render(par1Entity, par2, par3, par4, par5, par6, par7);
    }
}
