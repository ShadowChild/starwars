package shadowchild.mods.starwars.inventory;

import net.minecraft.inventory.InventoryBasic;


public class InventoryColourizer extends InventoryBasic {

    public ContainerColourizer container;

    public InventoryColourizer(ContainerColourizer container) {

        super("Colourizer", false, 1);
        this.container = container;
    }

    @Override
    public void markDirty() {

        super.markDirty();
        container.onCraftMatrixChanged(this);
    }
}
