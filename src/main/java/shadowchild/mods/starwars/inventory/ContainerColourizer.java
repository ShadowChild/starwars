package shadowchild.mods.starwars.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;


public class ContainerColourizer extends Container {

    final IInventory colourizerInventory = new InventoryColourizer(this);
    final World world;
    private final int guiOffest = 26;

    public ContainerColourizer(final InventoryPlayer inventoryPlayer, World world) {

        this.world = world;

        addSlotToContainer(new SlotColourizer(this, inventoryPlayer, 0, 11 + guiOffest, 17));
        int l;

        for(l = 0; l < 3; ++l) {
            for(int i1 = 0; i1 < 9; ++i1) {
                addSlotToContainer(new Slot(inventoryPlayer, i1 + l * 9 + 9, 17 + i1 * 18 + guiOffest, 91 + l * 18));
            }
        }

        for(l = 0; l < 9; ++l) {
            addSlotToContainer(new Slot(inventoryPlayer, l, 17 + l * 18 + guiOffest, 149));
        }

        for(int k = 0; k < 4; k++) {
            final int armorType = k;
            addSlotToContainer(new Slot(inventoryPlayer, 39 - k, 7, 24 + k * 19) {

                @Override
                public int getSlotStackLimit() {
                    return 1;
                }

                @Override
                public boolean isItemValid(ItemStack par1ItemStack) {
                    Item item = (par1ItemStack == null ? null : par1ItemStack.getItem());
                    return item != null && item.isValidArmor(par1ItemStack, armorType, inventoryPlayer.player);
                }
            });
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2) {
        ItemStack itemStack = null;
        final Slot slot = (Slot)inventorySlots.get(par2);

        if(slot != null && slot.getHasStack()) {
            final ItemStack stack = slot.getStack();

            final ItemStack tempStack = stack.copy();
            itemStack = stack.copy();
            tempStack.stackSize = 1;

            if(par2 != 0) {
                final Slot slot1 = (Slot)inventorySlots.get(0);

                if(!slot1.getHasStack() && slot1.isItemValid(tempStack) && mergeItemStack(tempStack, 0, 1, false)) {
                    stack.stackSize--;
                    itemStack = stack.copy();
                }

            } else if(!mergeItemStack2(stack, 1, 41, true)) {
                return null;
            }

            if(stack.stackSize == 0) {
                slot.putStack(null);
            } else {
                slot.onSlotChanged();
            }

            if(itemStack.stackSize == stack.stackSize) {
                return null;
            }
            slot.onPickupFromSlot(par1EntityPlayer, stack);
        }
        return itemStack;
    }

    private boolean mergeItemStack2(ItemStack stack, int i, int j, boolean b) {
        boolean flag1 = false;
        int k = i;

        if(b) {
            k = j - 1;
        }

        Slot slot;
        ItemStack itemstack1;

        if(stack.isStackable()) {
            while(stack.stackSize > 0 && (!b && k < j || b && k >= i)) {
                slot = (Slot)this.inventorySlots.get(k);
                itemstack1 = slot.getStack();

                if(itemstack1 != null && itemstack1 == stack && (!stack.getHasSubtypes() || stack.getItemDamage() == itemstack1.getItemDamage()) && ItemStack.areItemStackTagsEqual(stack, itemstack1)) {
                    int l = itemstack1.stackSize + stack.stackSize;

                    if(l <= stack.getMaxStackSize()) {
                        stack.stackSize = 0;
                        itemstack1.stackSize = l;
                        slot.onSlotChanged();
                        flag1 = true;
                    } else if(itemstack1.stackSize < stack.getMaxStackSize()) {
                        stack.stackSize -= stack.getMaxStackSize() - itemstack1.stackSize;
                        itemstack1.stackSize = stack.getMaxStackSize();
                        slot.onSlotChanged();
                        flag1 = true;
                    }
                }

                if(b) {
                    --k;
                } else {
                    ++k;
                }
            }
        }

        if(stack.stackSize > 0) {
            if(b) {
                k = j - 1;
            } else {
                k = i;
            }

            while(!b && k < j || b && k >= i) {
                slot = (Slot)this.inventorySlots.get(k);
                itemstack1 = slot.getStack();

                if(itemstack1 == null && slot.isItemValid(stack)) {
                    slot.putStack(stack.copy());
                    slot.onSlotChanged();
                    stack.stackSize = 0;
                    flag1 = true;
                    break;
                }

                if(b) {
                    --k;
                } else {
                    ++k;
                }
            }
        }

        return flag1;
    }

    @Override
    public void onCraftMatrixChanged(IInventory par1IInventory) {

        super.onCraftMatrixChanged(par1IInventory);
    }

    @Override
    public void onContainerClosed(EntityPlayer par1EntityPlayer) {

        super.onContainerClosed(par1EntityPlayer);

        for(int i = 0; i < colourizerInventory.getSizeInventory(); i++) {
            final ItemStack stack = colourizerInventory.getStackInSlot(i);
            if(stack != null) {
                if(par1EntityPlayer.inventory.addItemStackToInventory(stack)) {
                } else {
                    par1EntityPlayer.entityDropItem(stack, 1f);
                }
            }
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer var1) {

        return true;
    }
}
