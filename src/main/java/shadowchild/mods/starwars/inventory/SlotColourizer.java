package shadowchild.mods.starwars.inventory;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import shadowchild.mods.starwars.implement.IColourable;


public class SlotColourizer extends Slot {

    final ContainerColourizer container;

    public SlotColourizer(ContainerColourizer container, IInventory par1IInventory, int par2, int par3, int par4) {

        super(par1IInventory, par2, par3, par4);

        this.container = container;
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }

    @Override
    public boolean isItemValid(ItemStack par1ItemStack) {
        return par1ItemStack.getItem() instanceof IColourable;
    }
}
