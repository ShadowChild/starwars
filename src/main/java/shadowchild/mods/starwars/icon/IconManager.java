package shadowchild.mods.starwars.icon;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.event.TextureStitchEvent;


public class IconManager {

    public static final int HILT_INDEX = 0;
    public static final int BLADE_INDEX = 1;

    public static IIcon[] icons = new IIcon[16];

    @SubscribeEvent
    public void registerIcons(TextureStitchEvent.Pre evt) {

        if(evt.map.getTextureType() == 1) {

            // Items
            icons[HILT_INDEX] = evt.map.registerIcon("starwars:saberHilt");
            icons[BLADE_INDEX] = evt.map.registerIcon("starwars:saberBlade");
        } else if(evt.map.getTextureType() == 0) {

            // Blocks
        } else {

        }
    }
}
