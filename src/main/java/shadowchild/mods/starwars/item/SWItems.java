package shadowchild.mods.starwars.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import shadowchild.mods.starwars.StarWars;
import shadowchild.mods.starwars.implement.ILoader;

import java.util.ArrayList;
import java.util.List;


public class SWItems implements ILoader {

    private static final SWItems instance = new SWItems();
    public static Weapon blaster;
    public static Weapon lightSaber;
    private List<ItemStack> itemsList = new ArrayList<ItemStack>();

    public static List<ItemStack> getItemsList() {

        return instance.itemsList;
    }

    @Override
    public void load() {

        blaster = new WeaponBlaster();
        lightSaber = new ItemLightSaber();

        registerItems();
    }

    private void registerItems() {

        registerItem(blaster, "blaster");
        registerLightSaber();
    }

    private void registerItem(Item item, String itemName) {

        GameRegistry.registerItem(item, itemName, StarWars.MOD_ID);
        instance.itemsList.add(new ItemStack(item));
    }

    private void registerItemWithMeta(Item item, String itemName, int types) {

        for(int i = 0; i < types; i++) {

            GameRegistry.registerItem(item, itemName + "_" + i, StarWars.MOD_ID);
            instance.itemsList.add(new ItemStack(item, 1, i));
        }
    }

    private void registerLightSaber() {

        ItemStack stack  = new ItemStack(lightSaber);

        NBTTagCompound tagCompound = stack.getTagCompound();

        if(tagCompound == null) {

            stack.setTagCompound(new NBTTagCompound());
            tagCompound = stack.getTagCompound();
        }

        tagCompound.setInteger("ColourRed", 0);
        tagCompound.setInteger("ColourGreen", 0);
        tagCompound.setInteger("ColourBlue", 255);

        stack.setTagCompound(tagCompound);

        GameRegistry.registerItem(lightSaber, "lightSaber");
        instance.itemsList.add(stack);
    }
}
