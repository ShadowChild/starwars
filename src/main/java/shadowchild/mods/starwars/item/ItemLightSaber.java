package shadowchild.mods.starwars.item;

import shadowchild.mods.starwars.implement.IColourable;


public class ItemLightSaber extends Weapon implements IColourable {

    public ItemLightSaber() {

        setUnlocalizedName("lightSaber");
        setMaxStackSize(1);
    }
}
