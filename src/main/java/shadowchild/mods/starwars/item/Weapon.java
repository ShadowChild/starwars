package shadowchild.mods.starwars.item;

import co.uk.shadowchild.mc.core.common.item.AbstractItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import shadowchild.mods.starwars.util.LocalizationUtils;


public class Weapon extends AbstractItem {

    protected String unlocalized;

    public String getUnlocalizedName() {

        return LocalizationUtils.getWeaponName(unlocalized);
    }

    public String getUnlocalizedName(ItemStack par1ItemStack) {

        return LocalizationUtils.getWeaponName(unlocalized);
    }

    @Override
    public boolean isFull3D() {

        return true;
    }

    @Override
    public Item setUnlocalizedName(String par1Str) {

        this.unlocalized = par1Str;
        return this;
    }
}
