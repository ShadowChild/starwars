//package shadowchild.mods.starwars.packet;
//
//import cpw.mods.fml.common.network.Player;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.network.INetworkManager;
//import net.minecraft.util.ChatMessageComponent;
//import shadowchild.mods.starwars.data.FactionData;
//import shadowchild.mods.starwars.enums.EnumFactionType;
//
//import java.io.ByteArrayInputStream;
//import java.io.DataInputStream;
//
//
//public class PacketUpdateFaction extends PacketSW {
//
//    public PacketUpdateFaction(byte[] byteArray) {
//
//        super(byteArray);
//    }
//
//    @Override
//    public void execute(INetworkManager manager, Player player) {
//
//        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
//
//        EntityPlayer thePlayer = (EntityPlayer)player;
//
//        try {
//
//            int faction = dis.readInt();
//            boolean isSyncing = dis.readBoolean();
//
//            FactionData data = (FactionData)thePlayer.getExtendedProperties(FactionData.NAME);
//
//            switch(faction) {
//
//                case 0: {
//
//                    data.setFaction(EnumFactionType.NOT_ASSIGNED);
//                    break;
//                }
//
//                case 1: {
//
//                    data.setFaction(EnumFactionType.SITH);
//                    break;
//                }
//
//                case 2: {
//
//                    data.setFaction(EnumFactionType.REPUBLIC);
//                    break;
//                }
//
//                case 3: {
//
//                    data.setFaction(EnumFactionType.PRIVATEER);
//                    break;
//                }
//            }
//            if(!isSyncing) {
//                thePlayer.sendChatToPlayer(ChatMessageComponent.createFromText("Your faction is " + data.getFaction().name));
//            }
//        } catch(Exception e) {
//
//            e.printStackTrace();
//        }
//    }
//}
