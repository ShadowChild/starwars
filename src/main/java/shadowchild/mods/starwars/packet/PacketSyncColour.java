package shadowchild.mods.starwars.packet;

import co.uk.shadowchild.mc.core.common.packet.AbstractPacket;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S04PacketEntityEquipment;
import net.minecraft.server.MinecraftServer;
import shadowchild.mods.starwars.StarWars;


public class PacketSyncColour extends AbstractPacket {

    public ItemStack stack;
    public int red, green, blue;

    public PacketSyncColour() {}

    public PacketSyncColour(ItemStack stack, int red, int green, int blue) {

        this.stack = stack;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    @Override
    public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {

        ByteBufUtils.writeItemStack(buffer, this.stack);
        buffer.writeInt(red);
        buffer.writeInt(green);
        buffer.writeInt(blue);
    }

    @Override
    public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {

        this.stack = ByteBufUtils.readItemStack(buffer);
        this.red = buffer.readInt();
        this.green = buffer.readInt();
        this.blue = buffer.readInt();
    }

    @Override
    public void handleClientSide(EntityPlayer player) {

        handleServerSide(player);
    }

    @Override
    public void handleServerSide(EntityPlayer player) {

        StarWars.getLogger().info("Handling sync packet");

        NBTTagCompound tag = this.stack.getTagCompound();

        if(tag != null) {

            setNBT(tag);
        } else {

            NBTTagCompound newTag = stack.writeToNBT(new NBTTagCompound());
            setNBT(newTag);
        }
        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
        for(Object playerMP : server.getEntityWorld().playerEntities) {

            ((EntityPlayerMP)playerMP).playerNetServerHandler.sendPacket(new S04PacketEntityEquipment(player.getEntityId(), player.inventory.currentItem, this.stack));
        }
        //        ((EntityPlayerMP)player).playerNetServerHandler.sendPacket(new S04PacketEntityEquipment(player.getEntityId(), player.inventory.currentItem, this.stack));
    }

    private void setNBT(NBTTagCompound tag) {

        tag.setInteger("ColourRed", this.red);
        tag.setInteger("ColourGreen", this.green);
        tag.setInteger("ColourBlue", this.blue);
        this.stack.setTagCompound(tag);
    }
}
