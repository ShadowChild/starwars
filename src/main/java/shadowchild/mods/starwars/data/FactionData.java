//package shadowchild.mods.starwars.data;
//
//import net.minecraft.entity.Entity;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.nbt.NBTTagCompound;
//import net.minecraft.world.World;
//import net.minecraftforge.common.IExtendedEntityProperties;
//import shadowchild.mods.starwars.enums.EnumFactionType;
//import shadowchild.mods.starwars.packet.PacketSW;
//import shadowchild.mods.starwars.packet.PacketUpdateFaction;
//
//import java.io.ByteArrayOutputStream;
//import java.io.DataOutputStream;
//
//
//public class FactionData implements IExtendedEntityProperties {
//
//    public static final String NAME = "SWFaction";
//
//    public EntityPlayer player;
//    private static EnumFactionType faction = EnumFactionType.NOT_ASSIGNED;
//
//    public FactionData(EntityPlayer player) {
//
//        this.player = player;
//    }
//
//    @Override
//    public void saveNBTData(NBTTagCompound compound) {
//
//        NBTTagCompound tag = new NBTTagCompound();
//
//        tag.setString(NAME, faction.name);
//
//        compound.setTag("StarWars", tag);
//    }
//
//    @Override
//    public void loadNBTData(NBTTagCompound compound) {
//
//        NBTTagCompound tag = (NBTTagCompound)compound.getTag("StarWars");
//
//        String factionName = tag.getString(NAME);
//
//        for(EnumFactionType factionType : EnumFactionType.values()) {
//
//            if(factionType.name.equals(factionName)) {
//
//                faction = factionType;
//            }
//        }
//    }
//
//    @Override
//    public void init(Entity entity, World world) {
//
//    }
//
//    public static boolean hasFaction(EntityPlayer player) {
//
//        return faction != EnumFactionType.NOT_ASSIGNED;
//    }
//
//    public void syncExtendedProperties() {
//
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        DataOutputStream dos = new DataOutputStream(bos);
//        try {
//
//            dos.writeInt(faction.id);
//            dos.writeBoolean(true);
//            PacketSW packet = new PacketUpdateFaction(bos.toByteArray());
////            PacketDispatcher.sendPacketToPlayer(packet, (Player)player);
//        } catch(Exception e) {
//
//            e.getStackTrace();
//        }
//    }
//
//    public EnumFactionType getFaction() {
//
//        return faction;
//    }
//
//    public void setFaction(EnumFactionType faction) {
//
//        FactionData.faction = faction;
//        this.syncExtendedProperties();
//    }
//}
