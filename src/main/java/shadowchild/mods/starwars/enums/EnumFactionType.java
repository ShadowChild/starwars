package shadowchild.mods.starwars.enums;

public enum EnumFactionType {

    NOT_ASSIGNED(0, "not_assigned"),
    SITH(1, "Sith"),
    REPUBLIC(2, "Republic"),
    PRIVATEER(3, "Privateer");
    public int id;
    public String name;

    private EnumFactionType(int id, String name) {

        this.id = id;
        this.name = name;
    }
}
