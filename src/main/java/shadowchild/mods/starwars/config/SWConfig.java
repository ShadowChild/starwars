package shadowchild.mods.starwars.config;

import co.uk.shadowchild.mc.core.common.config.Configuration;
import cpw.mods.fml.common.FMLCommonHandler;
import org.lwjgl.input.Keyboard;
import shadowchild.mods.starwars.implement.ILoader;
import shadowchild.mods.starwars.util.Utils;

import java.io.File;
import java.io.IOException;


public class SWConfig implements ILoader {

    private static Configuration cfg;

    public SWConfig(File cfgDir) {

        try {

            this.cfg = new Configuration(Utils.getConfigFileForSide(FMLCommonHandler.instance().getSide(), cfgDir));
        } catch(IOException e) {

            e.printStackTrace();
        }
    }

    public static void set(String categoryName, String propertyName, String newValue) {

        try {

            cfg.set(categoryName, propertyName, newValue);
        } catch(IOException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void load() {

        try {

            //            cfg = new Configuration(Utils.getConfigFileForSide(FMLCommonHandler.instance().getSide(), cfgDir));

            Settings.keyBindColourizer = cfg.getOrCreateInt("Keybinds", "key.colourizer.code", Keyboard.KEY_V);
            //            cfg = new Configuration(new File(cfgDir, "/StarWars/settings.ini"));

            //            Settings.red = cfg.getOrCreateInt("Colours", "rgb.red.value", 0);
            //            Settings.green = cfg.getOrCreateInt("Colours", "rgb.green.value", 0);
            //            Settings.blue = cfg.getOrCreateInt("Colours", "rgb.blue.value", 255);
        } catch(Exception e) {

            e.printStackTrace();
        }
    }
}
