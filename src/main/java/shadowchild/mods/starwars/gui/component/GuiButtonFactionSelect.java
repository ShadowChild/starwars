package shadowchild.mods.starwars.gui.component;

import net.minecraft.client.gui.GuiButton;
import shadowchild.mods.starwars.enums.EnumFactionType;


public class GuiButtonFactionSelect extends GuiButton {

    public EnumFactionType faction;

    public GuiButtonFactionSelect(int id, int x, int y, int width, int height, String string, EnumFactionType faction) {

        super(id, x, y, width, height, string);
        this.faction = faction;
    }
}
