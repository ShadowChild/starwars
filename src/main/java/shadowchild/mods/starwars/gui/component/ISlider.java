package shadowchild.mods.starwars.gui.component;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


//If your GUI has sliders you need to implement this interface.
@SideOnly(Side.CLIENT)
public interface ISlider {

    void onChangeSliderValue(GuiSlider slider);
}
