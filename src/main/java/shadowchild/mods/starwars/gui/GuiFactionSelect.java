package shadowchild.mods.starwars.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.opengl.GL11;
import shadowchild.mods.starwars.enums.EnumFactionType;
import shadowchild.mods.starwars.gui.component.GuiButtonFactionSelect;


public class GuiFactionSelect extends GuiScreen {

    private EntityPlayer player;

    public GuiFactionSelect(EntityPlayer entityPlayer) {

        this.player = entityPlayer;
    }

    @Override
    public void drawScreen(int i, int j, float f) {

        drawDefaultBackground();

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        this.drawCenteredString(this.fontRendererObj, "Select Faction", width / 2, 30, 0xffffff);

        super.drawScreen(i, j, f);
    }

    @Override
    public void initGui() {

        this.buttonList.clear();

        int centerX = width / 2;

        this.buttonList.add(new GuiButtonFactionSelect(0, centerX - 150, 60, 300, 20, "Sith", EnumFactionType.SITH));
        this.buttonList.add(new GuiButtonFactionSelect(1, centerX - 150, 100, 300, 20, "Republic", EnumFactionType.REPUBLIC));
        this.buttonList.add(new GuiButtonFactionSelect(2, centerX - 150, 140, 300, 20, "Privateer", EnumFactionType.PRIVATEER));
    }

    @Override
    public void actionPerformed(GuiButton button) {

        GuiButtonFactionSelect factionButton = button instanceof GuiButtonFactionSelect ? (GuiButtonFactionSelect)button : null;

        if(factionButton != null) {

            //            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //            DataOutputStream dos = new DataOutputStream(bos);
            //            try {
            //
            //                dos.writeInt(factionButton.faction.id);
            //                dos.writeBoolean(false);
            //                Packet250CustomPayload packet = new PacketUpdateFaction(bos.toByteArray());
            //                PacketDispatcher.sendPacketToServer(packet);
            //            } catch(Exception e) {
            //
            //                e.printStackTrace();
            //            }

            // this.player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG).setString("SWFaction",
            // factionButton.faction.name);
            System.out.println("faction = " + factionButton.faction.name);
            this.mc.displayGuiScreen(null);
            this.mc.setIngameFocus();
        }
    }
}
