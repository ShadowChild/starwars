package shadowchild.mods.starwars.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import shadowchild.mods.starwars.inventory.ContainerColourizer;


public class GuiColourizer extends GuiContainer {

    public GuiColourizer(EntityPlayer player, World world) {

        super(new ContainerColourizer(player.inventory, world));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {

    }

    @Override
    public boolean doesGuiPauseGame() {

        return true;
    }
}
