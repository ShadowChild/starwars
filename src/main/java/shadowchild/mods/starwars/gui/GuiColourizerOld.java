package shadowchild.mods.starwars.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.inventory.InventoryBasic;
import org.lwjgl.opengl.GL11;
import shadowchild.mods.starwars.gui.component.GuiSlider;
import shadowchild.mods.starwars.gui.component.ISlider;


public class GuiColourizerOld extends GuiScreen implements ISlider {

    private final int xSizeOfTexture = 166;
    private final int ySizeOfTexture = 181;
    public InventoryBasic guiInventory;
    private GuiSlider redSlider, greenSlider, blueSlider;
    private GuiButton done;

    private GuiButton[] customButtonList;

    public GuiColourizerOld(InventoryBasic inventory) {

        super();
        this.guiInventory = inventory;
    }

    @Override
    public void drawScreen(int i, int j, float f) {

        drawDefaultBackground();

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        int posX = (this.width - xSizeOfTexture) / 2;
        int posY = ((this.height - ySizeOfTexture) / 2) + 32;

        // RenderHelper.drawString("LightSaber Colourizer", posX + 33, posY +
        // 14);
        drawString(this.fontRendererObj, "LightSaber Colourizer", posX + 33, posY + 14, 0xffffff);

        renderColourBox();

        super.drawScreen(i, j, f);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void initGui() {

        this.buttonList.clear();

        int posX = (this.width - xSizeOfTexture) / 2;
        int posY = ((this.height - ySizeOfTexture) / 2) + 32;

        int spacerY = 22;
        int spacerX = 1;

        redSlider = new GuiSlider(0, posX + 15, posY + 30 + (spacerY * 0), "Red ", 0, 255, getColourInt("red"), this);
        greenSlider = new GuiSlider(1, posX + 15, posY + 30 + (spacerY * 1), "Green ", 0, 255, getColourInt("green"), this);
        blueSlider = new GuiSlider(2, posX + 15, posY + 30 + (spacerY * 2), "Blue ", 0, 255, getColourInt("blue"), this);

        done = new GuiButton(4, posX + 46 - spacerX, posY + 30 + (spacerY * 3), 82, 20, "Save & Exit");

        customButtonList = new GuiButton[] {

                redSlider,
                greenSlider,
                blueSlider,
                done
        };

        for(GuiButton button : customButtonList) {

            this.buttonList.add(button);
        }
    }

    private int getColourInt(String s) {

        if(s.equals("red") || s.equals("green")) {

            return 0;
        }
        return 255;
    }

    @Override
    public boolean doesGuiPauseGame() {

        return true;
    }

    @Override
    public void actionPerformed(GuiButton button) {

        if(button.id == 4) {

            //            PacketSyncColour packet = new PacketSyncColour(mc.thePlayer.getCurrentEquippedItem(), red, green, blue);
            //
            //            StarWars.PIPELINE.sendToServer(packet);

            //            Settings.red = red;
            //            Settings.green = green;
            //            Settings.blue = blue;
            //
            //            SWConfig.set("Colours", "rgb.red.value", Integer.toString(red));
            //            SWConfig.set("Colours", "rgb.green.value", Integer.toString(green));
            //            SWConfig.set("Colours", "rgb.blue.value", Integer.toString(blue));
            //
            this.mc.thePlayer.closeScreen();
        }
    }

    @Override
    public void onChangeSliderValue(GuiSlider slider) {

        //        switch(slider.id) {
        //
        //            case 0: {
        //
        //                red = slider.getValueInt();
        //                break;
        //            }
        //
        //            case 1: {
        //
        //                green = slider.getValueInt();
        //                break;
        //            }
        //
        //            case 2: {
        //
        //                blue = slider.getValueInt();
        //                break;
        //            }
        //        }
    }

    private void renderColourBox() {

        ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft().gameSettings, 32, 32);

        int posX = (this.width - xSizeOfTexture) / 2;
        int posY = (this.height - ySizeOfTexture) / 2;

        posX += 68;

        int posX2 = posX + sr.getScaledWidth();
        int posY2 = posY + sr.getScaledHeight();

        //        RenderHelper.drawRectWithRGB(posX, posY, posX2, posY2, red, green, blue);
    }
}
