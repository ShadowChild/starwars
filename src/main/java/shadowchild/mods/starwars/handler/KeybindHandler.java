package shadowchild.mods.starwars.handler;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.settings.KeyBinding;
import shadowchild.mods.starwars.StarWars;
import shadowchild.mods.starwars.config.Settings;
import shadowchild.mods.starwars.util.GuiIDs;
import shadowchild.mods.starwars.util.LocalizationUtils;


/**
 * @Author ShadowChild.
 */
public class KeybindHandler {

    private static KeyBinding colourizer = new KeyBindingCustom(LocalizationUtils.getKeyUnlocalized("colourizer"), Settings.keyBindColourizer, LocalizationUtils.getKeyCat("starwars"), "key.colourizer.code");

    public static void init() {

        ClientRegistry.registerKeyBinding(colourizer);
        StarWars.getLogger().info("Registered Colourizer Keybind");
    }

    @SubscribeEvent
    public void keyPressed(InputEvent.KeyInputEvent evt) {

        //        System.out.println(colourizer.isPressed());
        //        StarWars.getLogger().info(colourizer.isPressed());

        if(colourizer.isPressed()) {

            StarWars.proxy.openClientGui(GuiIDs.COLOURIZER);
        }
        //        if (colourizer.isPressed())
        //            StarWars.proxy.openClientGui(GuiIDs.COLOURIZER);
    }
}
