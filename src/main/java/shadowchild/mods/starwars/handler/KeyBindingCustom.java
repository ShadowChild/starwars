package shadowchild.mods.starwars.handler;

import net.minecraft.client.settings.KeyBinding;
import shadowchild.mods.starwars.config.SWConfig;


public class KeyBindingCustom extends KeyBinding {

    final String propertyName;

    public KeyBindingCustom(String name, int keyCode, String keyCat, String propertyName) {

        super(name, keyCode, keyCat);

        this.propertyName = propertyName;
    }

    @Override
    public void setKeyCode(int newCode) {

        super.setKeyCode(newCode);

        SWConfig.set("Keybinds", propertyName, String.valueOf(newCode));
    }
}
