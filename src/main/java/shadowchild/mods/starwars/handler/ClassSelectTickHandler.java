//package shadowchild.mods.starwars.handler;
//
//import cpw.mods.fml.common.ITickHandler;
//import cpw.mods.fml.common.TickType;
//import net.minecraft.client.Minecraft;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.world.World;
//import shadowchild.mods.starwars.StarWars;
//import shadowchild.mods.starwars.util.GuiIDs;
//
//import java.util.EnumSet;
//
//
//public class ClassSelectTickHandler implements ITickHandler {
//
//    private int delay = 10;
//    private int tickCount = delay;
//
//    @Override
//    public void tickStart(EnumSet<TickType> type, Object... tickData) {
//
//    }
//
//    @Override
//    public void tickEnd(EnumSet<TickType> type, Object... tickData) {
//
//        Minecraft mc = Minecraft.getMinecraft();
//        EntityPlayer player = mc.thePlayer;
//        World world = mc.theWorld;
//        if(player != null && world != null) {
//
//            if(tickCount == 0) {
//
//                StarWars.proxy.openClientGui(GuiIDs.CLASS_SELECT, player);
//                tickCount = -1;
//            } else {
//
//                --tickCount;
//            }
//        }
//    }
//
//    @Override
//    public EnumSet<TickType> ticks() {
//
//        return EnumSet.of(TickType.PLAYER);
//    }
//
//    @Override
//    public String getLabel() {
//
//        return "StarWars_ClassSelectTickHandler";
//    }
//}
