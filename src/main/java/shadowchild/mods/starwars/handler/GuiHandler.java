package shadowchild.mods.starwars.handler;

import co.uk.shadowchild.mc.core.client.gui.GuiHandlerBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import shadowchild.mods.starwars.gui.GuiColourizer;
import shadowchild.mods.starwars.inventory.ContainerColourizer;
import shadowchild.mods.starwars.util.GuiIDs;


public class GuiHandler extends GuiHandlerBase {

    @Override
    protected Object getElement(int ID, EntityPlayer player, World world, int x, int y, int z, boolean isClient) {

        switch(ID) {

            case GuiIDs.COLOURIZER: {

                if(isClient) {

                    return new GuiColourizer(player, world);
                } else {

                    return new ContainerColourizer(player.inventory, world);
                }
            }
        }
        return null;
    }
}
