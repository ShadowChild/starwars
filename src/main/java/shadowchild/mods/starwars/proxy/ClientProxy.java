package shadowchild.mods.starwars.proxy;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import shadowchild.mods.starwars.gui.GuiColourizer;
import shadowchild.mods.starwars.handler.KeybindHandler;
import shadowchild.mods.starwars.icon.IconManager;
import shadowchild.mods.starwars.item.SWItems;
import shadowchild.mods.starwars.render.BlasterRifleItemRenderer;
import shadowchild.mods.starwars.render.LightSaberItemRenderer;
import shadowchild.mods.starwars.util.GuiIDs;


public class ClientProxy extends CommonProxy {

    @Override
    public void openClientGui(int guiID, Object... params) {

        switch(guiID) {

            case GuiIDs.COLOURIZER: {

                FMLCommonHandler.instance().showGuiScreen(new GuiColourizer(Minecraft.getMinecraft().thePlayer, Minecraft.getMinecraft().theWorld));
                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void registerEvents() {

        super.registerEvents();

        FMLCommonHandler.instance().bus().register(new KeybindHandler());
        MinecraftForge.EVENT_BUS.register(new IconManager());
    }

    @Override
    public void registerHandlers() {

        super.registerHandlers();

        //        TickRegistry.registerTickHandler(new ClassSelectTickHandler(), Side.CLIENT);
        //        KeyBindingRegistry.registerKeyBinding(new KeybindHandler());

        KeybindHandler.init();
        FMLCommonHandler.instance().bus().register(new KeybindHandler());

        MinecraftForgeClient.registerItemRenderer(SWItems.lightSaber, new LightSaberItemRenderer());
        MinecraftForgeClient.registerItemRenderer(SWItems.blaster, new BlasterRifleItemRenderer());
    }
}
