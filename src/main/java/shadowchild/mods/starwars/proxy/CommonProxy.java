package shadowchild.mods.starwars.proxy;

import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import shadowchild.mods.starwars.StarWars;
import shadowchild.mods.starwars.event.CommonEventHandler;
import shadowchild.mods.starwars.handler.GuiHandler;

import java.util.HashMap;
import java.util.Map;


public class CommonProxy {

    private Map<String, NBTTagCompound> entityPropertiesMap = new HashMap<String, NBTTagCompound>();

    public void openClientGui(int guiID, Object... params) {

    }

    public void registerEvents() {

        MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
    }

    public void registerHandlers() {

        NetworkRegistry.INSTANCE.registerGuiHandler(StarWars.instance, new GuiHandler());
    }
}
