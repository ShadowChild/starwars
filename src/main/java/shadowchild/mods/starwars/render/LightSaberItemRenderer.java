package shadowchild.mods.starwars.render;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import shadowchild.mods.starwars.icon.IconManager;


public class LightSaberItemRenderer implements IItemRenderer {

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {

        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {

        return false;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack stack, Object... data) {

        int red = 0, green = 0, blue = 255;

        try {

            red = stack.getTagCompound().getInteger("ColourRed");
            green = stack.getTagCompound().getInteger("ColourGreen");
            blue = stack.getTagCompound().getInteger("ColourBlue");
        } catch(NullPointerException e) {

            //            StarWars.getLogger().info("this isnt right");

            //            red = 0;
            //            green = 0;
            //            blue = 255;
        }

        float r = (float)red / 255.0F;
        float g = (float)green / 255.0F;
        float b = (float)blue / 255.0F;

        switch(type) {

            case EQUIPPED: {

                // Blade Icon
                RenderHelper.drawIconIn3D(stack, IconManager.icons[IconManager.BLADE_INDEX], r, g, b, 1F, false);

                // Hilt Icon
                RenderHelper.drawIconIn3D(stack, IconManager.icons[IconManager.HILT_INDEX], 1F, 1F, 1F, 1F, false);

                break;
            }

            case EQUIPPED_FIRST_PERSON: {

                // Blade Icon
                RenderHelper.drawIconIn3D(stack, IconManager.icons[IconManager.BLADE_INDEX], r, g, b, 1F, true);

                // Hilt Icon
                RenderHelper.drawIconIn3D(stack, IconManager.icons[IconManager.HILT_INDEX], 1F, 1F, 1F, 1F, true);

                break;
            }

            case INVENTORY: {

                // Blade Icon
                RenderHelper.renderIconInInventory(IconManager.icons[IconManager.BLADE_INDEX], r, g, b);

                // Hilt Icon
                RenderHelper.renderIconInInventory(IconManager.icons[IconManager.HILT_INDEX], 1F, 1F, 1F);

                break;
            }

            default: {

                break;
            }
        }
    }
}
