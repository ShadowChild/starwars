package shadowchild.mods.starwars.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;


/**
 * Author: ShadowChild.
 */
@SideOnly(Side.CLIENT)
public class RenderHelper {

    public static void drawRectWithRGBA(int posX, int posY, int posX2, int posY2, int red, int green, int blue, int alpha) {

        int j1;

        if(posX < posX2) {
            j1 = posX;
            posX = posX2;
            posX2 = j1;
        }

        if(posY < posY2) {
            j1 = posY;
            posY = posY2;
            posY2 = j1;
        }

        float a = (float)alpha / 255.0F;
        float r = (float)red / 255.0F;
        float g = (float)green / 255.0F;
        float b = (float)blue / 255.0F;
        Tessellator tessellator = Tessellator.instance;
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(r, g, b, a);
        tessellator.startDrawingQuads();
        tessellator.addVertex((double)posX, (double)posY2, 0.0D);
        tessellator.addVertex((double)posX2, (double)posY2, 0.0D);
        tessellator.addVertex((double)posX2, (double)posY, 0.0D);
        tessellator.addVertex((double)posX, (double)posY, 0.0D);
        tessellator.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawRectWithRGB(int posX, int posY, int posX2, int posY2, int red, int green, int blue) {

        drawRectWithRGBA(posX, posY, posX2, posY2, red, green, blue, 255);
    }

    // Use ItemStack and Icon to support block icons
    public static void drawIconIn3D(ItemStack stack, IIcon icon, float red, float green, float blue, float alpha, boolean firstPerson) {

        GL11.glPushMatrix();

        Minecraft mc = Minecraft.getMinecraft();

        if(icon == null || stack == null) {

            GL11.glPopMatrix();
            return;
        }

        mc.renderEngine.bindTexture(mc.renderEngine.getResourceLocation(stack.getItemSpriteNumber()));
        Tessellator tessellator = Tessellator.instance;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        // GL11.glTranslatef(-f4, -f5, 0.0F);
        float f6 = 1.5F;
        GL11.glScalef(f6, f6, f6);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(red, green, blue, alpha);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
        if(firstPerson) {

            GL11.glRotatef(5F, 0f, -1f, -0f);
            GL11.glTranslatef(0F, -0.19F, 0F);
        } else {

            GL11.glTranslatef(-0.25F, -0.1F, 0F);
        }
        ItemRenderer.renderItemIn2D(tessellator, icon.getMaxU(), icon.getMinV(), icon.getMinU(), icon.getMaxV(), icon.getIconWidth(), icon.getIconHeight(), 0.0625F);

        GL11.glPopMatrix();
    }

    public static void renderIconInInventory(IIcon icon, float red, float green, float blue) {

        RenderItem renderItem = new RenderItem();

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor3f(red, green, blue);

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);

        renderItem.renderIcon(0, 0, icon, 16, 16);
    }
}
