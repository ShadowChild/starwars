package shadowchild.mods.starwars.render;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import shadowchild.mods.starwars.model.ModelBlasterRifle;


/**
 * Author: ShadowChild.
 */
public class BlasterRifleItemRenderer implements IItemRenderer {

    public ModelBlasterRifle blaster = new ModelBlasterRifle();

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {

        return type != ItemRenderType.INVENTORY;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {

        return false;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {

        GL11.glPushMatrix();

        GL11.glScalef(0.01f, 0.01f, 0.01f);
        blaster.render();

        GL11.glPopMatrix();
    }
}
