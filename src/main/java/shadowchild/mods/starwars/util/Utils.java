package shadowchild.mods.starwars.util;

import cpw.mods.fml.relauncher.Side;
import net.minecraft.item.Item;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Utils {

    public static List<Item> getAllItems() {

        List<Item> list = new ArrayList<Item>();

        Iterator<?> iterator = Item.itemRegistry.iterator();

        while(iterator.hasNext()) {

            list.add((Item)iterator.next());
        }

        return list;
    }

    public static File getConfigFileForSide(Side side, File baseDir) {

        switch(side) {

            case CLIENT:
                return new File(baseDir, "StarWars/settings_client.ini");
            case SERVER:
                return new File(baseDir, "StarWars/settings_server.ini");
            default:
                return new File(baseDir, "StarWars/settings.ini");
        }
    }
}
