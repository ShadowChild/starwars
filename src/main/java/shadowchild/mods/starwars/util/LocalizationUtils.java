package shadowchild.mods.starwars.util;

public class LocalizationUtils {

    private static String getPrefix() {

        return "starwars:";
    }

    public static String getKeyUnlocalized(String keyName) {

        return getPrefix() + "keys." + keyName;
    }

    public static String getKeyCat(String catName) {

        return getPrefix() + "keycat." + catName;
    }

    public static String getItemName(String itemName) {

        return getPrefix() + "items." + itemName;
    }

    public static String getWeaponName(String weaponName) {

        return getPrefix() + "weapon." + weaponName;
    }
}
